  if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
  fi

#   Change Prompt
#   ------------------------------------------------------------
function git_branch {
   ref=$(git symbolic-ref HEAD 2> /dev/null) || return;
   echo "("${ref#refs/heads/}") ";
}

function git_since_last_commit {
   now=`date +%s`;
   last_commit=$(git log --pretty=format:%at -1 2> /dev/null) || return;
   seconds_since_last_commit=$((now-last_commit));
   minutes_since_last_commit=$((seconds_since_last_commit/60));
   hours_since_last_commit=$((minutes_since_last_commit/60));
   minutes_since_last_commit=$((minutes_since_last_commit%60));
   echo "${hours_since_last_commit}h${minutes_since_last_commit}m ";
}

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

DEBIAN_CROOT="${debian_chroot:+($debian_chroot)}"
PS_INFO="\[\033[01;31m\]\u\[\033[00m\]@\[\033[01;32m\]\h\[\033[00m\]:[\[\033[01;34m\]\w\[\033[00m\]] \[\033[0m\]\[\033[1;36m\]\$(git_branch)\[\033[0;33m\]\$(git_since_last_commit)\[\033[0m\]"
ARROW="➜"
PS1="${DEBIAN_CROOT}${PS_INFO}${ARROW} "

alias hdfs01="ssh 52.68.3.13"
alias hdfs02="ssh 52.193.6.179"
alias hdfs03="ssh 52.193.6.204"
alias ldap="ssh ldap.pubgame.com"